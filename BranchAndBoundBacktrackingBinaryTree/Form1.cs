﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BranchAndBoundBacktrackingBinaryTree
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            const int n = 4;
            const int m = 1;

            // целевая функция
            int[] vektor = new int[n] { 5, 3, 7, 6 };
            // предел
            string predel = "max"; // всегда max для задачи о ранце (max, min)

            // ограничения
            int[,] bound = new int[m, n] { { 1, 1, 4, 2 } };
            // знак ограничения ("<",">")
            string[] sign = new string[m] { "<" };
            // значение после равенства для ограничения
            int[] boundVal = new int[m] { 6 };

            // ветвления с указанием веса TODO: переделать под общий вид (для всех Xi = 1,0)
            int[][] branch = new int[n][];
            for (int i = 0; i < n; i++)
            {
                branch[i] = new int[2] { 1, 0 };
            }

            InParam param = new InParam(n, m, vektor, predel, bound, sign, boundVal, branch);

            BranchAndBoundBacktracking.TradicionMax(param);
        }

    }
}
