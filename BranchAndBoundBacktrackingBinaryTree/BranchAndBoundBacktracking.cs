﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BranchAndBoundBacktrackingBinaryTree
{
    class Node
    {
        public float weight;    // вес узла
        public int layer;       // уровень спуска
        public int?[] way;      // вектор переменных (пройденный путь)

        public Node() { }

        public Node(float weight, int layer, int?[] way)
        {
            this.weight = weight;
            this.layer = layer;
            this.way = way;
        }
    }

    // структура для передачи параметров задачи в функцию
    struct InParam
    {
        // размерность задачи
        public int n;
        // количество ограничений
        public int m;

        // целевая функция
        public int[] vektor;
        // предел (max, min)
        public string predel; // всегда max для задачи о ранце

        // ограничения
        public int[,] bound;
        // знак ограничения ("<",">")
        public string[] sign;
        // значение после равенства для ограничения
        public int[] boundVal;
        // ветвления с указанием веса TODO: переделать в одномерный массив (для всех Xi = 1,0)
        public int[][] branch;

        public InParam(int n, int m, int[] vektor, string predel, int[,] bound, string[] sign, int[] boundVal, int[][] branch)
        {
            this.n = n;
            this.m = m;
            this.vektor = vektor;
            this.predel = predel;
            this.bound = bound;
            this.sign = sign;
            this.boundVal = boundVal;
            this.branch = branch;
        }
    }

    class BranchAndBoundBacktracking
    {

        static public void TradicionMax(InParam param)
        {
            int n = param.n;
            int m = param.m;
            int[] vektor = param.vektor;
            string predel = param.predel;
            int[,] bound = param.bound;
            string[] sign = param.sign;
            int[] boundVal = param.boundVal;
            int[][] branch = param.branch;

            for (int i = 0; i < param.sign.Length; i++)
            {
                if (param.sign[i] == "<=") { boundVal[i] = param.boundVal[i] + 1; sign[i] = "<"; };
                if (param.sign[i] == ">=") { boundVal[i] = param.boundVal[i] - 1; sign[i] = ">"; };
            }


            // список висячих вершин
            List<Node> hangingNodes = new List<Node>();



            // создаем стартовую вершину
            Node myNode;
            Node result; // вершина которая будет результатом вычислений
            int?[] hlp = new int?[n];
            for (int i = 0; i < n; i++)
            {
                hlp[i] = null;
            }
            if (predel == "min")
            {
                myNode = new Node(int.MaxValue - 100, 0, hlp);
                hangingNodes.Add(myNode);
                // должен быть на единицу(100) больше для перебивки
                result = new Node(int.MaxValue, 0, hlp);
            }
            else
            {
                myNode = new Node(int.MinValue + 100, 0, hlp);
                hangingNodes.Add(myNode);
                // должен быть на единицу(100) меньше для перебивки
                result = new Node(int.MinValue, 0, hlp);
            }


            // Метка окончания работы
            bool isCompleat = false;
            int backtrack = 0; // 

            while (!isCompleat)   // итерация для спуска
            {
                if (hangingNodes.Count == 0) { isCompleat = true; };

                Console.WriteLine("===========================");


                if (backtrack == n) { backtrack--; };
                bool findNode = false; // метка существования вершины из которой будет спуск
                while (!findNode) // цикл нахождения вершины из которой будет спуск
                {
                    var help = from item in hangingNodes
                               where item.layer == backtrack
                               select item;
                    if (help.Count() == 0)
                    {
                        backtrack--;
                        if (backtrack == 0) { isCompleat = true; break; };
                    }
                    else
                    {
                        if (predel == "min") // находим минимальный вес из всех висячих вершин
                        {
                            myNode = help.Aggregate((i1, i2) => i1.weight < i2.weight ? i1 : i2);
                            if (myNode.weight < result.weight) { findNode = true; }
                            else { hangingNodes.Remove(myNode); };
                        }
                        else // находим максимальный вес из всех висячих вершин
                        {
                            myNode = help.Aggregate((i1, i2) => i1.weight > i2.weight ? i1 : i2);
                            if (myNode.weight > result.weight) { findNode = true; }
                            else { hangingNodes.Remove(myNode); };
                        }
                    }
                }
                if (isCompleat) { break; };
                // удаляем из спика узел по которому будем спускаться
                hangingNodes.Remove(myNode);




                for (int i = 0; i < branch[myNode.layer].Length; i++) // спуск для каждого значения Xi 
                {
                    int[] vektorKoeff = new int[n]; // коэфф которые надо найти для подсчета веса узла
                    for (int j = 0; j < n; j++)
                    {
                        if (predel == "min")
                        {
                            if (myNode.way[j] == null) { vektorKoeff[j] = branch[myNode.layer].Min(); }
                            else { vektorKoeff[j] = (int)myNode.way[j]; } // установка коэфф из значений пути
                        }
                        else
                        {
                            if (myNode.way[j] == null) { vektorKoeff[j] = branch[myNode.layer].Max(); }
                            else { vektorKoeff[j] = (int)myNode.way[j]; } // установка коэфф из значений пути
                        }

                        if (j == myNode.layer) { vektorKoeff[j] = branch[myNode.layer][i]; }; // спуск по 1й ветке

                    }

                    int[,] branchvektorKoeff = new int[m, n];
                    bool isKick = false;
                    for (int k = 0; k < m; k++)
                    {
                        int kick = 0; // для проверки подходит ли спуск по ограничению
                        for (int j = 0; j < n; j++)
                        {
                            if (sign[k] == "<")
                            {
                                if (myNode.way[j] == null) { branchvektorKoeff[k, j] = branch[myNode.layer].Min(); }
                                else { branchvektorKoeff[k, j] = (int)myNode.way[j]; } // установка коэфф из значений пути  
                            }
                            if (sign[k] == ">")
                            {
                                if (myNode.way[j] == null) { branchvektorKoeff[k, j] = branch[myNode.layer].Max(); }
                                else { branchvektorKoeff[k, j] = (int)myNode.way[j]; } // установка коэфф из значений пути
                            }
                            if (j == myNode.layer) { branchvektorKoeff[k, j] = branch[myNode.layer][i]; };
                            kick += branchvektorKoeff[k, j] * bound[k, j];
                        }
                        // проверкa подходит ли спуск по ограничению
                        if (sign[k] == "<" && kick >= boundVal[k]) { isKick = true; break; };
                        if (sign[k] == ">" && kick <= boundVal[k]) { isKick = true; break; };
                    }
                    // нужна ли эта вершина 
                    if (!isKick)
                    {
                        // создание и добавление новой весяч вершины
                        // расчет веса
                        int newWeight = 0;
                        for (int j = 0; j < n; j++)
                        {
                            newWeight += vektorKoeff[j] * vektor[j];
                        }
                        // расчет уровня
                        int newLayer = myNode.layer + 1;
                        // расчет пути
                        int?[] newWay = new int?[n];
                        Array.Copy(myNode.way, newWay, n);
                        newWay[myNode.layer] = branch[myNode.layer][i];

                        Node newNode = new Node(newWeight, newLayer, newWay);
                        hangingNodes.Add(newNode);

                        backtrack = myNode.layer + 1;

                        // запись промежуточного результататата
                        if (predel == "min")
                        {
                            if (newNode.layer == n)
                            {
                                if (newNode.weight < result.weight) { result = newNode; }
                                else { hangingNodes.Remove(newNode); };
                            }
                        }
                        else
                        {
                            if (newNode.layer == n)
                            {
                                if (newNode.weight > result.weight) { result = newNode; }
                                else { hangingNodes.Remove(newNode); };
                            }
                        }
                    }
                }




                // вывод весячих вершин в консоль
                foreach (Node item in hangingNodes)
                {
                    string help;
                    Console.WriteLine("----------------");
                    Console.WriteLine("Weight = " + item.weight);
                    Console.WriteLine("Layer = " + item.layer);
                    help = "";
                    for (int j = 0; j < item.way.Length; j++)
                    {
                        help += item.way[j].ToString() + ",";
                    }
                    Console.WriteLine("Way = " + help);
                }
            }

            string hl;
            Console.WriteLine(" ################ ");
            Console.WriteLine("Weight = " + result.weight);
            Console.WriteLine("Layer = " + result.layer);
            hl = "";
            for (int j = 0; j < result.way.Length; j++)
            {
                hl += result.way[j].ToString() + ",";
            }
            Console.WriteLine("Way = " + hl);
        }
    }
}
